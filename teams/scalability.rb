GROUP_ID = 000000
TOP_LEVEL_EPIC_ID = 000000

def show_team_output
  puts "SCALABILITY"
end

def team_description_heading(output)
  output << "Scaling GitLab.com is a continuous task. We create issues and epics as we encounter things, with the goal of addressing issues based on the [Scalability team Work Prioritisation process](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/#work-prioritization-process)."
  output << "Below you can find issues, epics and boards that drive our work."
  output << ""
  output << "Summary of issues not in Epics: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/538"
end

def team_description_footer(output)
  output << "## Open Work outside of Scalability Issue Tracker"
  output << "Due to a limitation in issues not being accessible to epics/boards in different groups, the epics and issue boards below are used to track work that the Scalability team creates."
  output << ""
  output << "- Board showing all gitlab-org issues : https://gitlab.com/groups/gitlab-org/-/boards/1477989"
  output << "- Summary of issues outside of the Scalability Issue Tracker: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/430"
end

def board_markdown_link_for_epic(id)
  "[Board](https://gitlab.com/gitlab-com/gl-infra/scalability/-/boards/1290868?epic_id=#{id})"
end

def additional_completed_output(output)
  pages_epic = Gitlab.epic(9970, 3980)
  output << "| #{pages_epic.title} <br/><br/> #{pages_epic.web_url} |  #{pages_epic.start_date[0..9]} | #{pages_epic.closed_at[0..9]} | #{get_status(pages_epic, false)} |"
end
