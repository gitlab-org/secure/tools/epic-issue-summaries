require 'gitlab'
require 'docopt'

docstring = <<DOCSTRING
Create summary of issues not in epics

Usage:
  #{__FILE__} --token=<token> --team=<team> --project_id=<project_id> --summary_issue_id=<summary_issue_id> [--group_by_scoped_label=<group_by_scoped_label>] [--dry-run]
  #{__FILE__} -h | --help

Options:
  -h --help                                       Show this screen.
  --token=<token>                                 Personal access token
  --team=<team>                                   Team name for scoped label
  --project_id=<project_id>                       Project ID for the summary
  --summary_issue_id=<summary_issue_id>           Issue to write summary in description
  --group_by_scoped_label=<group_by_scoped_label> Issue to write summary in description
  --dry-run                                       Display result without editing issue description
DOCSTRING

def get_label_from_prefix(labels, prefix)
  workflow_label = ""
  labels.each do |label|
    if label.include?(prefix)
      workflow_label = label
    end
  end

  if workflow_label != ""
    "~\"#{workflow_label}\" "
  else
    ""
  end

end

def get_output_line(issue)

  title = issue.confidential ? "Confidential Issue" : issue.title

  web_url = issue.web_url
  workflow_status = get_label_from_prefix(issue.labels, "workflow-infra")
  board = get_label_from_prefix(issue.labels, "board")
  service = get_label_from_prefix(issue.labels, "Service")

  result = "| #{title} <br/>#{web_url} | #{service} | #{board} | #{workflow_status} |"
end

def get_output_lines_for_scoped_label(output, scoped_label, issues)

  all_scoped_labels = []

  issues.each do |i|
    i.labels.each do |l|
      if(l.include?(scoped_label+"::"))
        all_scoped_labels << l
      end
    end
  end

  scoped_labels = issues.map {|i| i.labels }.flatten.uniq.select{|l| l.include?(scoped_label+"::")}.sort!

  result = issues.group_by {|i| (i.labels & scoped_labels)[0] }

  scoped_labels.each do |label|
    grouped_issues = result[label]

    output << "## #{label}"
    output << ""
    output << "Issues: #{grouped_issues.size} ~\"#{label}\""
    output << ""
    output << "| **Topic** | **#{label}** | **Board** | **Workflow Status** | "
    output << "|-----------|------------ | --------- | --------------------|"

    grouped_issues.each do |issue|
      if(issue.labels.include?("#{label}"))
        output << get_output_line(issue)
      end
    end
  end
end

def get_output_lines_without_scoped_label(output, scoped_label, issues)

  output << "## Other"
  output << ""
  output << "| **Topic** | **Board** | **Workflow Status** | "
  output << "|-----------| ----------| --------------------|"

  issues.each do |issue|

    if(!issue.labels.any? { |s| s.include?(scoped_label) } && !issue.labels.any? { |s| s.include?('team-tasks') })
      output << get_output_line(issue)
    end
  end
end

def get_output_lines_for_team_tasks(output, issues)

  results = issues.select{ |i| i.labels.include?("team-tasks")}

  output << "## Team Tasks"
  output << ""
  output << "Issues: #{results.size} ~team-tasks "
  output << ""
  output << "| **Topic** | **Service** | **Board** |**Workflow Status** | **Due Date**  |"
  output << "|-----------|------------ | ----------| -------------------| ------------- |"

  results.each do |issue|
      output << "#{get_output_line(issue)} #{issue.due_date}|"
  end
end

begin

  options = Docopt::docopt(docstring)

  token = options.fetch('--token')

  Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.com/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT']
    config.private_token  = token                       # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
  end

  project_id = options.fetch('--project_id')
  summary_issue_id = options.fetch('--summary_issue_id')
  team = options.fetch('--team')
  dry_run = options.fetch('--dry-run', false)
  group_by_scoped_label = options.fetch('--group_by_scoped_label', nil)


  all_issues = []
  current_page = 1

  loop do

    api_results = Gitlab.issues(project_id, {scope:"all", labels:"team::#{team}", state:"opened", page:current_page, per_page:20})
    break if api_results.length == 0
    all_issues.concat(api_results.select{|i| i.epic_iid == nil })
    current_page += 1
  end


  total_issue_count = all_issues.size

  output = []
  output << "Summary of issues that are not in an Epic"
  output << ""
  output << "Total Issues: #{total_issue_count}"
  output << ""

  get_output_lines_for_team_tasks(output, all_issues)

  if(group_by_scoped_label)
    get_output_lines_for_scoped_label(output, group_by_scoped_label, all_issues)
    get_output_lines_without_scoped_label(output, group_by_scoped_label, all_issues)

  else
    output << "## Other"
    output << ""
    output << "| **Topic** | **Service** | **Board** | **Workflow Status** | "
    output << "|-----------|------------ | --------- | ----------|"

    all_issues.each do |issue|

      if(!issue.labels.any? { |s| s.include?('team-tasks') })
        output << get_output_line(issue)
      end
    end

  end
  new_description = output.join("\n")

  if(dry_run)
    puts new_description
  else
    puts "Writing to issue"
    Gitlab.edit_issue(project_id, summary_issue_id, {description: new_description})
  end

end

