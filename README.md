# Epic and Issue Summaries

This project is a collection of scripts that can be used to create summaries for epic and issues.  

To get started:
- clone the project
- run `bundle`

## Scripts

### Epic Summary

From the epic provided, this script creates a summary of all child epics.

Required input is:
- API token
- team

This can be run with `--dry-run` which will output the new description without editing it.

The `team` supplied will be used to find the team specific information from the `/team/` directory.  
 

### Issues Not In Epics

This script creates a summary of all issues that are not associated with an epic. 

Required input is:
- API token
- team name (as would appear on the `team::` scoped label)
- project id
- summary issue id (where the summary will be written to)
- group by scoped label

This can be run with `--dry-run` which will output the new description without editing it. 

Note: This currently has text specific for the Scalability Team.

### Epic Issue Relationships

This script generates a [Mermaid diagram] of the relationships between issues in a given epic and adds it into the 
status block in the description on the epic. 

The epic must have an existing mermaid tag that will be replaced by this script.  

[Mermaid diagram]: https://mermaid-js.github.io/mermaid/

Required input is:
- API token
- group id
- epic id (optional; if not given, this searches for in-progress epics for the Scalability team that do not have any child epics)

This can be run with `--dry-run` which will output the Mermaid source without editing any epic descriptions.

Diagram key:
- 🎯 - exit criterion
- ✅ - closed issue
- ❌ - blocking / blocked-by issue not in epic
- ⏳ - assigned and open issue

### Credits

This repo is a public copy of scripts originally written by @smcgivern and @rnienaber in https://gitlab.com/gitlab-com/gl-infra/epic-issue-summaries