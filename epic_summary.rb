require 'gitlab'
require 'docopt'

require './lib/gitlab_client/epics'

docstring = <<DOCSTRING
Create summary of child epics

Usage:
  #{__FILE__} --token=<token> --team=<team> [--dry-run]
  #{__FILE__} -h | --help

Options:
  -h --help                 Show this screen.
  --token=<token>           Personal access token
  --team=<team>             Team specific text to be applied 
  --dry-run                 Display result without editing epic description
DOCSTRING

def get_status_line(description, use_br)
  status = ""

  if description!= nil && description.index("## Status")

    end_location = description.length

    if description.index("mermaid")
      end_location = description.index("mermaid")-6
    end

    status = description[description.index("## Status")+10..end_location]
  end

  if use_br
    status.gsub!("\n", "<br/>")
  else
    status.gsub!("\n", " ")
  end

  status
end

def get_status(epic, look_for_children)
  status = get_status_line(epic.description, true)

  if look_for_children

    children = Gitlab.epic_epics(epic.group_id, epic.iid)

    if children.size > 0
      status = status + "<br/><br/>**Nested Epics: #{children.size}**"
    end

    status = status + "<br/>"

    children.each do |child|
      status = status + "<br/>**#{child.title}** #{child.web_url}: #{get_status_line(child.description, false)} <br/>"
    end

  end

  status
end

def get_dri(description)
  dri = ""

  if description.index("DRI")
    dri = description[description.index("DRI")+4..description.index("DRI")+50]
  end

  dri.split[0]
end


begin

  options = Docopt::docopt(docstring)

  token = options.fetch('--token')

  Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.com/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT']
    config.private_token  = token                       # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
  end

  team = options.fetch('--team')
  dry_run = options.fetch('--dry-run', false)

  require "./teams/#{team}"
  show_team_output
  group_id = GROUP_ID
  epic_id = TOP_LEVEL_EPIC_ID

  epic = Gitlab.epic(group_id, epic_id)
  child_epics = Gitlab.epic_epics(group_id, epic.iid)

  open_epics = child_epics.select {|e| e.state == "opened"}
  closed_epics = child_epics.select {|e| e.state == "closed"}

  output = []

  team_description_heading(output)

  output << ""
  output << "## Project Work"
  output << ""

  output << "### :hourglass: Work In Progress"
  output << "These epics are currently in progress. ~\"workflow-infra::In Progress\""
  output << ""
  output << "| **Topic** | **Links** | **Start Date** | **Target&nbsp;End&nbsp;Date** | **Summary** |"
  output << "|-----------|-----------|----------------|-------------------------------|-------------|"

  open_epics.sort_by!{|e| e.start_date ? e.start_date[0..9] : "9"}

  open_epics.each do |e|
    if e.labels.include?("workflow-infra::In Progress")
      puts e.title
      target_date = e.due_date ? e.due_date[0..9] : ""
      output << "| #{e.title} <br/> #{get_dri(e.description)} | #{e.web_url} <br/> #{board_markdown_link_for_epic(e.id)} | #{e.start_date[0..9]}| #{target_date} | #{get_status(e, true)}|"
    end
  end


  output << ""
  output << ""
  output << "### :arrow_forward: Next"
  output << "These are the epics we will be focusing on next. ~\"workflow-infra::Proposal\""
  output << ""
  output << "| **Topic** | **Links** | **Target Start Date** | **Summary** |"
  output << "|-----------|-----------|-----------------------|-------------|"

  open_epics.each do |e|
    if e.labels.include?("workflow-infra::Proposal")
      target_start_date = e.start_date ? e.start_date[0..9] : ""
      output << "| #{e.title} <br/> #{get_dri(e.description)} | #{e.web_url} <br/> #{board_markdown_link_for_epic(e.id)} | #{target_start_date} | #{get_status(e, true)}|"
    end
  end



  output << ""
  output << ""
  output << "### :stop_button: Other"
  output << "Epics that are currently being triaged.  ~\"workflow-infra::Triage\""
  output << ""
  output << "| **Topic** | **Links** | **Summary** |"
  output << "|-----------|-----------|-------------|"

  open_epics.each do |e|
    if e.labels.include?("workflow-infra::Triage")
      output << "| #{e.title} | #{e.web_url} <br/> #{board_markdown_link_for_epic(e.id)} | #{get_status(e, false)}|"
    end
  end

  output << "### :white_check_mark: Completed Work"
  output << "Items that have been completed. ~\"workflow-infra::Done\""
  output << "<details>"
  output << ""
  output << "| **Topic** | **Started** | **Ended** | **Summary**  |"
  output << "|-----------| ------------| ----------| ------------ |"

  closed_epics.sort_by!{|e| e.start_date ? e.start_date[0..9] : "9"}

  puts "-----"

  closed_epics.each do |e|

    puts e.title

    if e.labels.include?("workflow-infra::Done")
      output << "| #{e.title} <br/> <br/> #{e.web_url} | #{e.start_date[0..9]} | #{e.closed_at[0..9]} | #{get_status(e, true)} |"
    end
  end

  additional_completed_output(output)

  output << "</details>"

  output << ""
  output << ""
  output << "### :x: Cancelled Work"
  output << "Epics that were cancelled. ~\"workflow-infra::Cancelled\""
  output << "<details>"
  output << ""
  output << "| **Topic** | **Ended** | **Summary**  |"
  output << "|-----------| ----------| ------------ |"

  closed_epics.sort_by!{|e| e.closed_at ? e.closed_at[0..9] : "9"}

  closed_epics.each do |e|
    if e.labels.include?("workflow-infra::Cancelled")
      output << "| #{e.title} <br/> <br/> #{e.web_url} | #{e.closed_at[0..9]} | #{get_status(e, false)} |"
    end
  end

  output << ""
  output << ""

  team_description_footer(output)

  output << ""
  output << ""


  new_description = output.join("\n")

  if(dry_run)
    puts "------------------------------------------------------------"
    puts new_description
  else
    Gitlab.edit_epic(group_id, epic_id, {description: new_description})
  end

end
