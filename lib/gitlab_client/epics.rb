require 'gitlab'

class Gitlab::Client
  module Epics
    def epics(group_id, label, options: {})
      get("/groups/#{group_id}/epics?labels=#{label}", query: { per_page: 100 }.merge(options))
    end

    def epic_issues(group_id, epic_iid)
      get("/groups/#{group_id}/epics/#{epic_iid}/issues", query: { per_page: 100 })
    end

    def epic_epics(group_id, epic_iid)
      get("/groups/#{group_id}/epics/#{epic_iid}/epics", query: { per_page: 100 })
    end
  end
end

module Gitlab
  class Client < API
    include Epics
  end
end
