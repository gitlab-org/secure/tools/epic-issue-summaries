#!/usr/bin/env ruby

require 'docopt'
require 'gitlab'
require 'json'
require 'set'

require './lib/gitlab_client/epics'

MERMAID_REGEX = /\n(## Status .+?)\n```mermaid.+\n```/m

docstring = <<DOCSTRING
Add a Mermaid diagram of issue relationships to a leaf epic

Usage:
  #{__FILE__} --token=<token> --groupid=<groupid> [--epicid=<epicid>] [--dry-run]
  #{__FILE__} -h | --help

Options:
  -h --help                 Show this screen
  --token=<token>           Personal access token
  --groupid=<groupid>       Group ID for the epic
  --epicid=<epicid>         Epic ID; if set, only operate on one epic
  --dry-run                 Display result without editing epic description
DOCSTRING

def id(issue)
  "issue#{issue['iid']}"
end

def key_fields(issue)
  ['id', 'iid', 'web_url', 'title'].to_h { |f| [f, issue[f]] }
end

def update_mermaid(token:, group_id:, epic_id:, dry_run:)
  in_epic = Set.new
  from_relations = Set.new
  relations = Set.new
  mermaid = ['graph TD']
  original_description = Gitlab.epic(group_id, epic_id).description

  unless original_description =~ MERMAID_REGEX
    if epic_id.to_i == 526
      binding.irb
    end
    puts "#{epic_id} does not have a Mermaid diagram"
    return
  end

  Gitlab.epic_issues(group_id, epic_id).each do |issue|
    iid = issue['iid']
    graph_id = id(issue)

    in_epic << key_fields(issue)

    title = "##{iid}"
    title = "🎯 #{title}" if issue['labels'].include?('exit criterion')
    if issue['state'] == 'closed'
      title = "✅ #{title}"
    elsif issue['assignees'].any?
      title = "⏳ #{title}"
    end

    mermaid << "  #{graph_id}[\"#{title}\"]"
    mermaid << "  click #{graph_id} \"#{issue['web_url']}\" \"#{issue['title'].gsub('"', "'")}\""

    Gitlab.issue_links(issue['project_id'], issue['iid']).each do |link|
      case link['link_type']
      when 'is_blocked_by'
        source = id(link)
        destination = graph_id
      when 'blocks'
        source = graph_id
        destination = id(link)
      else
        next
      end

      from_relations << key_fields(issue)
      from_relations << key_fields(link)

      unless relations.include?([source, destination])
        mermaid << "  #{source} --> #{destination}"

        relations << [source, destination]
      end
    end
  end

  (from_relations - in_epic).each do |extra_issue|
    mermaid << "  #{id(extra_issue)}[\"❌ ##{extra_issue['iid']}\"]"
    mermaid << "  click #{id(extra_issue)} \"#{extra_issue['web_url']}\" \"#{extra_issue['title'].gsub('"', "'")}\""
  end

  mermaid_string = mermaid.join("\n")

  if dry_run
    puts '-' * 70
    puts epic_id
    puts ''
    puts mermaid_string
    puts '-' * 70
    puts ''
  else
    new_description = original_description
                        .gsub(MERMAID_REGEX,
                              "\n\\1\n```mermaid\n#{mermaid_string}\n```\n")

    Gitlab.edit_epic(group_id, epic_id, description: new_description)
  end
end

options = Docopt::docopt(docstring)
token = options.fetch('--token')
group_id = options.fetch('--groupid')
epic_id = options.fetch('--epicid', nil)
dry_run = options.fetch('--dry-run', false)

Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  config.private_token = token
end

if epic_id
  update_mermaid(token: token, group_id: group_id, epic_id: epic_id, dry_run: dry_run)
else
  Gitlab.epics(group_id, 'workflow-infra::In Progress,team::Scalability', options: { state: 'opened' }).each do |epic|
    if Gitlab.epic_epics(epic['group_id'], epic['iid']).count == 0
      update_mermaid(token: token, group_id: group_id, epic_id: epic['iid'], dry_run: dry_run)
    end
  end
end
